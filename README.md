# React Boilerplate
Already project configured with the better technologies and practices.

Developed by [Bemobile](https://bemobile.tech/) for developers

<br />

## Used technologies

- Atomic Design (folders structures)
- TypeScript
- React 18
- React Router Dom (V6)
- Craco
- ESLint
- Prettier
- Redux Toolkit
- Git commit message linter
- Git scripts

<br />

