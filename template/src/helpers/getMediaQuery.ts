interface GetMediaQueryParams {
	breakpoint: string
	rule?: 'min' | 'max'
	dimension?: 'width' | 'height'
}

export function getMediaQuery({
	dimension = 'width',
	rule = 'min',
	breakpoint,
}: GetMediaQueryParams) {
	return `@media(${rule}-${dimension}: ${breakpoint}px)`
}
