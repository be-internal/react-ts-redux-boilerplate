import styled, { css } from 'styled-components'

export const ErrorContainer = styled.div`
	${({ theme }) => css`
		margin: 16px 0;

		span.error {
			font-weight: ${theme.font.weight.bold};
			color: ${theme.colors.red[900]};
		}
	`}
`
