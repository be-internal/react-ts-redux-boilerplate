/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react'

import { ErrorContainer } from './styles'

interface ErrorBoundaryProps {
	children: React.ReactNode
	getError?: (error: any, errorInfo: any) => void
	fallback?: React.ReactNode
}

interface ErrorBoundaryState {
	hasError: boolean
}

export default class ErrorBoundary extends React.Component<
	ErrorBoundaryProps,
	ErrorBoundaryState
> {
	constructor(props: ErrorBoundaryProps) {
		super(props)
		this.state = { hasError: false }
	}

	static getDerivedStateFromError() {
		return { hasError: true }
	}

	componentDidCatch(error: any, errorInfo: any) {
		this.props.getError && this.props.getError(error, errorInfo)
	}

	render() {
		if (this.state.hasError) {
			return (
				this.props.fallback || (
					<ErrorContainer>
						<p>
							Ocorreu um <span className='error'>erro</span> durante a
							inicialização desse componente =(
						</p>
						<p>Caso o erro persista, entre em contato conosco</p>
					</ErrorContainer>
				)
			)
		}

		return this.props.children
	}
}
