import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux'

import { App } from '@pages/App'
import { store } from '@store'
import { GlobalStyles } from '@styles/globalTheme'
import { theme } from '@styles/theme'
import { ThemeProvider } from 'styled-components'

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)

/* The Strict Mode is triggering useEffect twice on 
   development mode because test of future new features
*/
root.render(
	<React.StrictMode>
		<Provider store={store}>
			<ThemeProvider theme={theme}>
				<App />
				<GlobalStyles />
			</ThemeProvider>
		</Provider>
	</React.StrictMode>
)
