import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface ExampleState {
	show: boolean
	options: { title: string; message: string }
}

const initialState: ExampleState = {
	show: false,
	options: {
		title: '',
		message: '',
	},
}

export const examopleSlice = createSlice({
	name: 'example',
	initialState,
	reducers: {
		show: (
			state,
			{
				payload: { title, message },
			}: PayloadAction<{ title: string; message: string }>
		) => {
			state.options = initialState.options

			state.options = { title, message }
			state.show = true
		},

		hide: (state) => {
			state.show = false
		},
	},
})

export const examopleCreators = examopleSlice.actions

export const examopleReducer = examopleSlice.reducer
