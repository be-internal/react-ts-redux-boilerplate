export const theme = {
	colors: {
		primary: {
			500: '#5984c0',
		},
		red: {
			900: '#FF0000',
		},
	},
	font: {
		family: {
			primary: '"Roboto", sans-serif',
		},
		size: {
			small: '',
		},
		weight: {
			bold: '700',
			medium: '500',
			normal: '400',
		},
	},
	breakpoints: {
		small: '',
	},
}
