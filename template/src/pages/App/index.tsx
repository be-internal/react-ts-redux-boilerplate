import React from 'react'

import { ErrorBoundary } from '@components/atoms'

function Test() {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let test: any = {}

	if (Math.random() !== 1) {
		test = undefined
	}

	return (
		<p>
			Edit <code>src/App.tsx</code> and save to reload. {test.text}
		</p>
	)
}

export function App() {
	return (
		<div>
			<ErrorBoundary>
				<Test />
			</ErrorBoundary>

			<header>
				<a href='https://reactjs.org' target='_blank' rel='noopener noreferrer'>
					Learn React
				</a>
			</header>
		</div>
	)
}
