import React from 'react'
import { Navigate, Route, Routes, useLocation } from 'react-router-dom'

import { App } from '@pages/App'

type RouteType = Array<{
	path: string
	element: () => JSX.Element
	isPrivate: boolean
}>

type PrivateRouteProps = {
	children: React.ReactElement | null
}

function PrivateRoute({ children }: PrivateRouteProps) {
	const location = useLocation()

	const signed = true

	return signed ? (
		children
	) : (
		<Navigate to='/login' state={{ redirect: location.pathname }} />
	)
}

const routes: RouteType = [
	{
		path: '/',
		element: App,
		isPrivate: true,
	},
]

export function AppRoutes() {
	return (
		<Routes>
			{routes.map((route) => {
				if (route.isPrivate) {
					return (
						<Route
							key={route.path}
							{...route}
							element={
								<PrivateRoute>
									<route.element />
								</PrivateRoute>
							}
						/>
					)
				}

				return <Route key={route.path} {...route} element={<route.element />} />
			})}

			<Route path='*' element={<Navigate to='/' />} />
		</Routes>
	)
}
